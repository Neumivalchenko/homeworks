import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Collectors;

public class Homework_20 {
    public static void main(String[] args) {


        try(BufferedReader reader = new BufferedReader(new FileReader("input.txt"))){

            System.out.println("Номера автомобилей черного цвета: ");

            reader.lines()
                    .filter(line -> line.contains("Black"))
                    .map(line -> line.split("\\|"))
                    .forEach(line -> System.out.println(Arrays.toString(new String[]{line[0]})));

        } catch (Exception ignored){}


        try (BufferedReader priceReader = new BufferedReader(new FileReader("input.txt"))){

            System.out.println("Количество уникальных моделей стоимостью от 700 000 до 800 000: ");

            int size = (int) priceReader.lines()
                    .distinct()
                    .map(line -> line.split("\\|"))
                    .filter(line -> Integer.parseInt(line[4]) >= 700000 && Integer.parseInt(line[4]) <= 800000)
                    .count();

            System.out.println(size);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
