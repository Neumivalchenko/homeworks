public class LinkedList<T> {
    private static class Node<T>{
        private T value;
        private Node<T> next;

        public Node(T value) {
            this.value = value;
        }

        public Node<T> getNext() {
            return next;
        }
    }

    private Node<T> first;
    private Node<T> last;
    private int size = 0;

    public void add(T element){
        Node<T> newNode = new Node<>(element);
        if(size == 0){
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        size++;
    }

    public void addToBegin(T element){
        Node<T> newNode = new Node<>(element);
        if(size == 0){
            last = newNode;
        } else {
            newNode.next = first;
        }
        first = newNode;
        size++;
    }

    public T get(int index){
        Node<T> current = new Node<T>((T)first);
        current.next = first.next;
        if(index < 0 || index > size){
            System.err.println("Неверный индекс!");
        } else {
            for (int i = 0; i < index; i++) {
                current = current.getNext();
            }
        }
        System.out.println(current.value);
        return current.value;
    }



}
