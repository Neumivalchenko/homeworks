import java.util.Arrays;

public class ArrayList<T> {
    private static final int DEFAULT_SIZE = 10;

    private T[] elements;
    private int size;

    public ArrayList() {
        this.elements = (T[]) new Object[DEFAULT_SIZE];
        this.size = 0;
    }

    public void add(T element) {
        if (isFullArray()) {
            resize();
        }
        this.elements[size] = element;
        size++;
    }

    public boolean isFullArray() {
        return ((size + 1) > elements.length);
    }

    public void resize() {                       //увеличивает массив в полтора раза
        T[] oldElement = (T[]) new Object[elements.length + (elements.length / 2)];
        System.arraycopy(elements, 0, oldElement, 0, size);
        this.elements = oldElement;
    }

    public T get(int index) {                    //возвращает выбранный элемент массива либо null, если индекс некорректный
        if (isCorrectIndex(index)) {
            return elements[index];
        } else return null;
    }

    private boolean isCorrectIndex(int index) {  //проверка корректности введеного индекса
        return (index >= 0) && (index < size);
    }

    /**
     * Если индекс корректный - уменьшает массив на 1 путем удаления выбранного элемента
     *
     * @param index
     */
    public void removeAt(int index) {
        if (isCorrectIndex(index)) {
            T[] temp = (T[]) new Object[size];
            if (index == size - 1) {
                temp = Arrays.copyOfRange(elements, 0, elements.length - 1);
            } else if (index > 0) {
                System.arraycopy(elements, 0, temp, 0, index);
                System.arraycopy(elements, index + 1, temp, index, elements.length - (index + 1));
            } else temp = Arrays.copyOfRange(elements, 1, elements.length - 1);

            elements = temp;
            size--;
        } else System.err.println("Некорректный индекс!");
    }




    public int getSize() {
        return size;
    }
}


