import java.util.Scanner;
import java.util.Arrays;

public class main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Human[] humans = new Human[10];

        System.out.println("Добро пожаловать в сортировщик людей по весу!");
        System.out.println("Пожалуйста, следуйте следующим указаниям:");
        for (int i = 0; i < humans.length; i++) {
            System.out.println("Введите имя человека №" + (i + 1));
            String nameReader = scanner.nextLine();
            humans[i].setName(nameReader);
            System.out.println("Введите вес человека №" + (i + 1));
            int weightReader = scanner.nextInt();
            while (weightReader <= 0) {
                System.out.println("Вы ввели некорректный вес, повторите ввод");
            }
            humans[i].setWeight(weightReader);
        }
        Arrays.sort(humans);
        for (int i = 0; i < 5; i++) {
            System.out.print("   Имя: " + humans[i].getName() + ", вес: " + humans[i].getWeight());
        }
        System.out.println();
        for (int i = 5; i < 10; i++) {
            System.out.print("   Имя: " + humans[i].getName() + ", вес: " + humans[i].getWeight());
        }
    }




    public static void sortHumansWeight(Human[] humans){
        Human[] sort = new Human[10];
        int min = humans[0].getWeight();
        int indexOfMin;

        for (int i = 0; i < 10; i++) {
            sort[i].setWeight(humans[i].getWeight());
        }

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (min > humans[j].getWeight()){
                    min = humans[j].getWeight();
                    indexOfMin = j;
                }
            }
            sort[i].setWeight(min);
        }


            }
        }


