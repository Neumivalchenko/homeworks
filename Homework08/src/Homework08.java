import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Homework08 {
    public static void main(String[] args) throws IOException {
        BufferedReader scanner = new BufferedReader(new InputStreamReader(System.in));
        Human[] humans = new Human[10];

        System.out.println("Добро пожаловать в сортировщик людей по весу!");
        System.out.println("Пожалуйста, следуйте следующим указаниям:");
        for (int i = 0; i < humans.length; i++) {
            humans[i] = new Human();
            System.out.println("Введите имя человека №" + (i + 1));
            String nameReader = scanner.readLine();
            System.out.println("Введите вес человека №" + (i + 1));
            int weightReader = Integer.parseInt((scanner.readLine()));
            humans[i].setName(nameReader);
            humans[i].setWeight(weightReader);
        }

        sortHumansWeight(humans);

        for (int i = 0; i < 5; i++) {
            System.out.println("   Имя: " + humans[i].getName() + ", вес: " + humans[i].getWeight());
        }

        for (int i = 5; i < 10; i++) {
            System.out.println("   Имя: " + humans[i].getName() + ", вес: " + humans[i].getWeight());
        }
        System.out.println("Ваши человеки отсортированы");
    }


    public static void sortHumansWeight(Human[] humans){
        Human[] temp = new Human[10];
        int indexOfMin = 0;



        for (int i = 0; i < 9; i++) {
            for (int j = 8; j >= 0; j--) {
                if (humans[j].getWeight() > humans[j + 1].getWeight()){
                    temp[j] = humans[j + 1];
                    humans[j + 1] = humans[j];
                    humans[j] = temp[j];
                }
            }

        }


    }
}

