public class Logger {
    private static final Logger instance;
    static {
        instance = new Logger();
    }
    private Logger() {
        this.message = new String[MAX_VALUE_OF_LOGGER];
    }

    private String[] message;
    private static final int MAX_VALUE_OF_LOGGER = 100;
    private int count = 0;


    void log(String message){
        if(count < MAX_VALUE_OF_LOGGER){
            this.message[count] = message;
            count++;
        } else System.err.println("log is overflow");

        System.out.println("Welcome to log! There is something message for you");
        System.out.println("Вы ввели: ");
        for (int i = 0; i < count; i++) {
            System.out.println(this.message[i]);
        }
        System.out.println("You are awesome!");
    }

    public static Logger getInstance() {
        return instance;
    }
}
