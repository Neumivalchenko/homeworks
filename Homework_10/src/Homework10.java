public class Homework10 {
    public static void main(String[] args) {

        Movable[] movables;

        Square square1 = new Square(5, 5, 23);
        Circle circle = new Circle(35, 29, 28);
        Square square = new Square(19, 27, 45);
        Rectangle rectangle = new Rectangle(12, 14, 23, 23);
        Ellipse ellipse = new Ellipse(21,25, 12.1, 10);

        Move move = new Move();

        move.addFigures(square1);
        move.addFigures(square);
        move.addFigures(circle);

        System.out.println("circle x:" + circle.getX() + " circle y:" + circle.getY());
        System.out.println("square x:" + square.getX() + " square y:" + square.getY());
        System.out.println("square1 x:" + square1.getX() + " square1 y:" + square1.getY());

        move.getMoveTo(15.565, 1156.54);

        System.out.println();
        System.out.println("circle x:" + circle.getX() + " circle y:" + circle.getY());
        System.out.println("square x:" + square.getX() + " square y:" + square.getY());
        System.out.println("square1 x:" + square1.getX() + " square1 y:" + square1.getY());


//        System.out.println("Circle perimeter is " + circle.getPerimeter());
//        System.out.println("Rectangle perimeter is " + rectangle.getPerimeter());
//        System.out.println("Square perimeter is " + square.getPerimeter());
//        System.out.println("Ellipse perimeter is " + ellipse.getPerimeter());
    }
}
