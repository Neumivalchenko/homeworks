public class Rectangle extends Figure{

    private double a;
    private double b;

    public Rectangle(double a, double b,  double x, double y){
        super(x, y);
        this.a = a;
        this.b = b;
    }


    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getPerimeter() {
        return a * b;
    }

    public void setA(double a) {
        this.a = a;
    }

    public void setB(double b) {
        this.b = b;
    }
}
