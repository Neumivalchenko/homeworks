public class Ellipse extends Figure{

    private double radius1;
    private double radius2;
    protected static final double Pi = 3.14;
    public Ellipse(double radius1, double radius2, double x, double y){
        super(x, y);
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    public double getRadius1() {
        return radius1;
    }

    public void setRadius1(double radius1) {
        this.radius1 = radius1;
    }

    public double getRadius2() {
        return radius2;
    }

    public void setRadius2(double radius2) {
        this.radius2 = radius2;
    }

    public double getPerimeter(){
        return ((4 * (Pi * radius1 * radius2 + (radius1-radius2)) / radius1+radius2));
    }
}
