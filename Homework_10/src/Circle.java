public class Circle extends Ellipse implements Movable {

    public Circle(double radius1, double x, double y) {
        super(radius1, radius1, x, y);
    }

    @Override
    public double getPerimeter() {
        return Pi * getRadius1() * getRadius1();
    }


    @Override
    public void getMoveTo(double x, double y) {
        setX(x);
        setY(y);
    }

    @Override
    public double getRadius1() {
        return super.getRadius1();
    }

    @Override
    public void setRadius1(double radius1) {
        super.setRadius1(radius1);
    }

    @Override
    public double getRadius2() {
        return super.getRadius2();
    }

    @Override
    public void setRadius2(double radius2) {
        super.setRadius2(radius2);
    }

    @Override
    public double setX(double x) {
        return super.setX(x);
    }

    @Override
    public double setY(double y) {
        return super.setY(y);
    }

    @Override
    public double getX() {
        return super.getX();
    }

    @Override
    public double getY() {
        return super.getY();
    }
}



