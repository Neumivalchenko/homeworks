public class Square extends Rectangle implements Movable{

    public Square(double x, double y, double a) {
        super(x, y, a, a);

    }

    @Override
    public double setX(double x) {
        return super.setX(x);
    }

    @Override
    public double setY(double y) {
        return super.setY(y);
    }

    @Override
    public double getX() {
        return super.getX();
    }

    @Override
    public double getY() {
        return super.getY();
    }

    @Override
    public void getMoveTo(double x, double y) {
        setX(x);
        setY(y);
    }

}
