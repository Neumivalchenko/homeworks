public abstract class Figure {
    private double x;
    private double y;
    public static final int MAX_VALUE_OF_COUNT = 10;
    protected Movable[] movables;
    protected int count;

    public Figure(){
        this.movables = new Movable[MAX_VALUE_OF_COUNT];
        this.count = 0;
    }

    public void addFigures(Movable figure) {
        if (this.count < MAX_VALUE_OF_COUNT) {
            this.movables[count] = figure;

            count++;
        }
        else System.err.println("Превышен лимит");
    }

    public Figure(double x, double y) {
        this.x = x;
        this.y = y;
    }

//    public abstract double getPerimeter();

    public double setX(double x) {
        this.x = x;
        return x;
    }

    public double setY(double y) {
        this.y = y;
        return y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}


