package com.company;


public class Main {
    /**
     * возвращает индекс числа массива на входе
     * @param i искомое число в массиве
     * @param array массив,в котором ищем заданное число
     * @return индекс искомого числа
     */
    public static int getIndex(double array[], double i) {      //getIndex для double
        for (int j = 0; j < array.length; j++) {
            if(array[j] == i)
                return j;
        }
        return -1;
    }

    public static int getIndex(int array[], int i) {    //getIndex для int
        for (int j = 0; j < array.length; j++) {
            if(array[j] == i)
                return j;
        }
        return -1;
    }

    /**
     * Процедура сортирует массив, размещая все нули в конец
     * @param array
     */
     public static void digitMoveLeft(int array[]){  //digitMoveLeft для int
        int j = 0;
        int a = array.length - 1;
        int[] arr;
        arr = new int[a + 1];
         for (int i = 0; i < array.length; i++) {
             if(array[i] != 0){
                 arr[j] = array[i];
                 j++;
             }
             if(array[i] == 0){
                 arr[a] = array[i];
                 a--;
            }
        }
         for (int i = 0; i < array.length; i++) {
             array[i] = arr[i];
         }

    }

    public static void digitMoveLeft(double array[]){  //digitMoveLeft для Double
        int j = 0;
        int a = array.length - 1;
        double[] arr;
        arr = new double[a + 1];
        for (int i = 0; i < array.length; i++) {
            if(array[i] != 0){
                arr[j] = array[i];
                j++;
            }
            if(array[i] == 0){
                arr[a] = array[i];
                a--;
            }
        }
        for (int i = 0; i < array.length; i++) {
            array[i] = arr[i];
        }

    }

    public static void print(int array[]){          //выводит содержимое массива (для упрощения проверки)
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
	int[] a = {0, 2, 0, 54, 0 , 0, 10, 1, 0, 0};
    digitMoveLeft(a);
        print(a);
    }
}
