public class Ellipse extends Figure{

    protected double radius1;
    protected double radius2;

    public Ellipse(double radius1, double radius2){
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    public double getPerimeter(){
        double pi = 3.14;
        return ((4 * (pi * radius1 * radius2 + (radius1-radius2)) / radius1+radius2));
    }
}
