public class Homework09 {
    public static void main(String[] args) {

        Circle circle = new Circle(35.2);
        Rectangle rectangle = new Rectangle(12, 14);
        Square square = new Square(24);
        Ellipse ellipse = new Ellipse(12.1, 10);

        System.out.println("Circle perimeter is " + circle.getPerimeter());
        System.out.println("Rectangle perimeter is " + rectangle.getPerimeter());
        System.out.println("Square perimeter is " + square.getPerimeter());
        System.out.println("Ellipse perimeter is " + ellipse.getPerimeter());
    }
}
