import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

public class Homework25 {
    public static void main(String[] args) {
        DataSource dataSource = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/postgres", "postgres","0000");
        ProductRepository productRepository = new ProductRepositoryJdbcTemplateImpl(dataSource);
        System.out.println(productRepository.findAllByPrice(120.50));
}
}
