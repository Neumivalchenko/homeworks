-- база товаров
create table product(
                     id serial primary key,
                     description varchar(40),
                     price double precision check (price > 0),
                     count integer check (count >= 0)
);


-- база клиентов
create table customer(
                         id serial primary key,
                         first_name varchar(20),
                         last_name varchar(20)
);

-- база заказов
create table orders(
                       customerId integer,
                       productId integer,
                       orderDate varchar(20),
                       countOfProducts integer check (orders.countOfProducts >= 0),
                       foreign key (customerId) references customer(id),
                       foreign key (productId) references product(id)
);


--сбор информации
insert into orders(customerId, productId, orderDate, countOfProducts) values (1, 3, '20-12-2020',40);
insert into orders(customerId, productId, orderDate, countOfProducts) values (2, 1, '15-02-2020',56);
insert into orders(customerId, productId, orderDate, countOfProducts) values (3, 4, '12-12-2020',80);
insert into orders(customerId, productId, orderDate, countOfProducts) values (1, 2, '10-12-2020',34);                                                                       1);
insert into orders(customerId, productId, orderDate, countOfProducts) values (2, 3, '15-06-2020',28);
insert into orders(customerId, productId, orderDate, countOfProducts) values (1, 3, '20-12-2020',90);
insert into orders(customerId, productId, orderDate, countOfProducts) values (2, 1, '15-02-2020',47);
insert into orders(customerId, productId, orderDate, countOfProducts) values (3, 4, '12-12-2020',36);
insert into orders(customerId, productId, orderDate, countOfProducts) values (1, 2, '10-12-2020',20);                                                                       1);
insert into orders(customerId, productId, orderDate, countOfProducts) values (3, 3, '15-06-2020',200);

insert into customer(first_name, last_name) values ('John', 'Snow');
insert into customer(first_name, last_name) values ('Stanis', 'Baratheon');
insert into customer(first_name, last_name) values ('Addart', 'Stark');

insert into product(description, price, count) values ('Tomatoes', 120.50, 900);
insert into product(description, price, count) values ('Pickles', 90.85, 400);
insert into product(description, price, count) values ('Bread', 15.00, 800);
insert into product(description, price, count) values ('Potatoes', 35.99, 1500);


select *
from product
where price = 15.00;