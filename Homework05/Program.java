import java.util.Scanner;

public class Program {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt(); //считываем ввод пользователя
        int minDigit = 9; //устанавливаем максимум из минимальных чисел
        int i; //переменная для хранения разряда (остатка от деления на 10)

       
        while (a != -1) {   //проверка ввода на заданный ограничитель
        	 if(a == 0){
        	minDigit = 0;
        };
            while (a != 0) { //цикл для отделения цифр от числа
                i = a % 10;
                if (minDigit > i) { //запись минимума в пременную minDigit
                    minDigit = i;
                }
                a = a / 10; //переход к следующему разряду

            }
        a = scanner.nextInt(); //считывание следующего числа
       }
        System.out.println(minDigit);
    }
}