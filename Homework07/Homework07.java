import java.util.Scanner;

public class Homework07 {


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int reader;
        int[] index = new int[200];     //массив для хранения количества вхождений одинаковых чисел
        int[] variables = new int[200]; //массив для сравнения чисел последовательности
        variables[0] = -100;

        for (int i = 0; i < 200; i++) {
            index[i] = 0;               //устанавливаем считчик вхождений для каждого числа на 0
        }

        for (int i = 1; i < 200; i++) {
            if (variables[i] == -1) {
                variables[i]++;
            }
            variables[i] = variables[i - 1] + 1;     //заполняем массив числами, присутствующими в последовательности
        }

        do {
            reader = scanner.nextInt();
            index[searchIndex(variables, reader)] += 1;     // находим индекс числа, полученного на входе, в массиве(variables)
        } while (reader != -1);                             // и увеличиваем соответствующий ему счетик в массиве index на 1

        System.out.println(variables[searchMin(index)]);    // выводим на консоль переменную, которая встречалась
    }                                                       // наименьшее количество раз в полученной последовательности

    /**
     * Ищет число(elementToSearch) в массиве(arr) и возвращает его индекс, способ поиска - бинарный.
     * @param arr
     * @param elementToSearch
     * @return
     */
    public static int searchIndex(int[] arr, int elementToSearch) {
        int left = 0;
        int right = arr.length - 1;

        while (left <= right) {
            int middle = (left + right) / 2;
            if (arr[middle] == elementToSearch) {
                return middle;
            } else if (arr[middle] < elementToSearch) {
                left = middle + 1;
            } else if (arr[middle] > elementToSearch) {
                right = middle - 1;
            }
        }
        return -1;
    }

    /**
     * ищет минимальное число в массиве(arr) и возвращает его индекс.
     * @param arr
     * @return
     */
    public static int searchMin(int[] arr) {
        int min = 1;
        int indexOfMin = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != 0) {
                min = arr[i];
                indexOfMin = i;
                break;
            }
        }
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < min && arr[i] != 0) {
                min = arr[i];
                indexOfMin = i;
            }
        }
        return indexOfMin;
    }
}




