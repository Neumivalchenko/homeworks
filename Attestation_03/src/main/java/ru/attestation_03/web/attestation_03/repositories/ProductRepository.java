package ru.attestation_03.web.attestation_03.repositories;

import ru.attestation_03.web.attestation_03.models.Product;

import java.util.List;

public interface ProductRepository {
    List<Product> findAll();
    void save(Product product);
    List<Product> findAllByPrice(double price);
}

