package ru.attestation_03.web.attestation_03;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Attestation03Application {

    public static void main(String[] args) {
        SpringApplication.run(Attestation03Application.class, args);
    }

}
