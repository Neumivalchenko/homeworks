package ru.attestation_03.web.attestation_03.repositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.attestation_03.web.attestation_03.models.Product;

import javax.sql.DataSource;
import java.util.List;


@Component
public class ProductRepositoryJdbcTemplateImpl implements ProductRepository {

    //language=SQL
    private static final String SQL_INSERT = "insert into product(description, price, count) values (?,?,?)";

    //language=SQL
    private static final String SQL_SELECT_ALL_PRODUCTS = "select * from product order by id";

    //language=SQL
    private static final String SQL_SELECT_ALL_BY_PRICE = "select * from product where price = ?";


    private JdbcTemplate jdbcTemplate;

    @Autowired
    public ProductRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> productRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
        String description = row.getString("Description");
        double price = row.getDouble("Price");
        int count = row.getInt("Count");

        return new Product(id, description, price, count);
    };



    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL_PRODUCTS, productRowMapper);
    }

    @Override
    public void save(Product product) {
        jdbcTemplate.update(SQL_INSERT, product.getDescription(), product.getPrice(), product.getCount());
    }

    @Override
    public List<Product> findAllByPrice(double price) {

        return jdbcTemplate.query(SQL_SELECT_ALL_BY_PRICE, new Object[] {price}, productRowMapper);
    }

}
