package ru.attestation_03.web.attestation_03.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.attestation_03.web.attestation_03.models.Product;
import ru.attestation_03.web.attestation_03.repositories.ProductRepository;

@Controller
public class ProductsController {
    @Autowired
    ProductRepository productRepository;

    @PostMapping("/product")
    public String addProduct(@RequestParam ("description") String description,
                             @RequestParam("price") double price,
                             @RequestParam("count") int count){

        Product product = Product.builder()
                .description(description)
                .price(price)
                .count(count)
                .build();

        productRepository.save(product);

        return "redirect:/products_add.html";
    }
}
