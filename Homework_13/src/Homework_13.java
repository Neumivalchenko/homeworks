import java.util.Arrays;

public class Homework_13 {
    public static void main(String[] args) {
        int[] test = {135, 256, 0, 689, 155324, 206, 101, 0, 2541};
        int[] evenDigits = Sequence.filter(test, number -> (number % 2) == 0); //массив с четными числами
        System.out.println(Arrays.toString(evenDigits));

        int[] sumOfDigitsIsEven = Sequence.filter(test, number -> (sumOfDigits(number) % 2 == 0)); //массив с числами, сума цифр которых четная
        System.out.println(Arrays.toString(sumOfDigitsIsEven));
    }

    public static int sumOfDigits(int number){
        int sumOfDigits = 0;
        while (number != 0){
            sumOfDigits += number % 10;
            number = number / 10;
        }
        return sumOfDigits;
    }  //Суммируем все цифры числа на входе и возвращает сумму
}
