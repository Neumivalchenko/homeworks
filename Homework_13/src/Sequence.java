import java.util.Arrays;

public class Sequence {
    public static int[] filter(int[] array, ByCondition condition) { //возвращает массив со всеми, соответствующими условию числами
        int[] temp = new int[array.length];
        int count = 0;

        for (int i = 0; i < array.length; i++) {
            if(condition.isOk(array[i]) && array[i] != 0){
                temp[count] = array[i];
                count++;
            }
        }
        return Arrays.copyOf(temp, count);
    }
}


