public class NumbersUtil {
    public int gcd(int a, int b){
        if(a < 0 || b < 0){
            return gcd(Math.abs(a), Math.abs(b));
        } else if (a == 0 || b == 0){
            throw new ArithmeticException();
        } else if (a == 1 || b == 1) return 1;

        if(a > b) {
            a %= b;
            return a == 0 ? b : gcd(a, b);
        } else if (a == b) {
            return a;
        } else {
            b %= a;
            return b == 0 ? a : gcd(a, b);
        }
    }
}
