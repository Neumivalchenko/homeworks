import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.shadow.com.univocity.parsers.annotations.Nested;

import static org.junit.jupiter.api.Assertions.*;


@DisplayName(value = "NumbersUtil is working when")
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
class NumbersUtilTest {

    private final NumbersUtil numbersUtil = new NumbersUtil();

        @Nested
        @ParameterizedTest(name = "return {2} on gcd {0} and {1}")
        @CsvSource(value = {"8, 4 , 4", "18, 12, 6", "9, 12, 3", "64, 48, 16"})
        public void return_correct_gcd (int a, int b, int result){
            assertEquals(result, numbersUtil.gcd(a, b));
        }

        @ParameterizedTest(name = "return {2} on gcd {0} and {1}")
        @CsvSource(value = {"8, -4 , 4", "-18, 12, 6", "9, -12, 3", "-64, -48, 16"})
        public void return_correct_with_negative_numbers (int a, int b, int result){
            assertEquals(result, numbersUtil.gcd(a, b));
        }


         @ParameterizedTest(name = "return {2} on gcd {0} and {1}")
         @CsvSource(value = {"9, 25 , 1", "35, 88, 1", "5, 3, 1", "7, 8, 1"})
         public void return_gcs_is_1 (int a, int b, int result){
             assertEquals(result, numbersUtil.gcd(a, b));
         }

         @ParameterizedTest(name = "return exception on gcd {0} and {1}")
         @CsvSource(value = {"8, 0", "0, 12", "0, 0", "67, 0"})
         public void return_arithmeticExeption_division_by_zero (int a, int b){
             assertThrows(ArithmeticException.class, () -> numbersUtil.gcd(a, b));
    }



}