import java.io.*;
import java.util.*;
import java.util.stream.Stream;

public class UsersRepositoryFileImpl implements UsersRepository {
    private String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }


    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();

        try (Reader reader = new FileReader(fileName); BufferedReader bufferedReader = new BufferedReader(reader)) {

            String line = bufferedReader.readLine();

            while (line != null) {
                String[] parts = line.split("\\|");
                int id = Integer.parseInt(parts[0]);
                String name = parts[1];
                int age = Integer.parseInt(parts[2]);
                boolean isWorker = Boolean.parseBoolean(parts[3]);

                User newUser = new User(id, name, age, isWorker);
                users.add(newUser);

                line = bufferedReader.readLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return users;
    }

    @Override
    public void save(User user) {

        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)){


            bufferedWriter.write(user.getId() + "\\|" + user.getName() + "\\|" + user.getAge() + "\\|" + user.isWorker());
            bufferedWriter.newLine();
            bufferedWriter.flush();

        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }



    @Override
    public List<User> findByAge(int age) {
        List<User> users = new ArrayList<>();

        try (Reader reader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(reader)){

            String line = bufferedReader.readLine();

            while (line != null) {

                String[] parts = line.split("\\|");
                //проверяем в текущей строке на совпадение по возрасту и если совпало - создаем нового User
                if (Integer.parseInt(parts[1]) == age) {
                    User newUser = new User(Integer.parseInt(parts[0]), parts[1], Integer.parseInt(parts[2]), Boolean.parseBoolean(parts[3]));
                    users.add(newUser);

                } else line = bufferedReader.readLine();
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return users;
    }

    @Override
    public List<User> findByIsWorkerIsTrue() {
        List<User> users = new ArrayList<>();

        try (Reader reader = new FileReader(fileName); BufferedReader bufferedReader = new BufferedReader(reader)){

            String line = bufferedReader.readLine();

            while (line != null) {
                String[] parts = line.split("\\|");
                //проверяем в текущей строке на совпадение постатусу и если совпало - создаем нового User
                if (Boolean.parseBoolean(parts[3])) {
                    User newUser = new User(Integer.parseInt(parts[0]), parts[1], Integer.parseInt(parts[2]), Boolean.parseBoolean(parts[3]));
                    users.add(newUser);
                } else line = bufferedReader.readLine();
                line = bufferedReader.readLine();
            }
        } catch (Exception e) {
            throw new IllegalArgumentException(e) {
            };
        }
        return users;
    }

    @Override
    public User findById(int id) {
        List<User> users = findAll();
        for (User user : users) {
            if (user.getId() == id) {
                return user;
            }
        }
        System.err.println("Пользователь не найден!");
        return null;
    }


    @Override
    public void update(User user) {
        List<User> users = findAll();
        users.removeIf(i -> i.equals(findById(user.getId())));
        users.add(user);

        try (Writer writer = new FileWriter("users.txt", false);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            for (User p : users) {
                bufferedWriter.write(p.getId() + "|" + p.getName() + "|" + p.getAge() + "|" + p.isWorker());
                bufferedWriter.newLine();
                bufferedWriter.flush();
            }
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }
}

