import java.util.List;

public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");
        User user = usersRepository.findById(2);
        user.setName("Саня");
        user.setAge(28);
        usersRepository.update(user);
    }
}

