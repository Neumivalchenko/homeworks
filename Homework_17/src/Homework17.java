import java.util.*;

public class Homework17 {
    public static void main(String[] args) {
        String string = "раз два раз два двадцать пять пятнадцать ноль пятьдесят два тридцать восемь ноль";
        inputCountOfWords(string);

    }

    private static void inputCountOfWords(String string) {
        Map<String, Integer> maxCountOfEqualsWords = new HashMap<>();


        String[] words = string.split(" ");

        for (String word : words){
            if(maxCountOfEqualsWords.containsKey(word)){
                maxCountOfEqualsWords.put(word, maxCountOfEqualsWords.get(word) + 1);
            } else maxCountOfEqualsWords.put(word, 1);
        }

        Set<Map.Entry<String, Integer>> entries = maxCountOfEqualsWords.entrySet();

        for(Map.Entry<String, Integer> entry : entries){
            System.out.println(entry.getKey() + " - " + entry.getValue());
        }
    }

}
