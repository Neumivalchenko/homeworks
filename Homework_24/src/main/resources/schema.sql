-- база товаров
create table good(
    id serial primary key,
    description varchar(40),
    price integer check (price > 0),
    count integer check (count >= 0)
);

-- база клиентов
create table customer(
    id serial primary key,
    first_name varchar(20),
    last_name varchar(20)
);

-- база заказов
create table orders(
    customerId integer,
    goodId integer,
    orderDate varchar(20),
    numbersOfGoods integer check (numbersOfGoods >= 0),
    foreign key (customerId) references customer(id),
    foreign key (goodId) references good(id)
);


--сбор информации
insert into orders(customerId, goodId, orderDate, numbersOfGoods) values (1, 3, '20-12-2020',1);
insert into orders(customerId, goodId, orderDate, numbersOfGoods) values (2, 1, '15-02-2020',1);
insert into orders(customerId, goodId, orderDate, numbersOfGoods) values (3, 4, '12-12-2020',3);
insert into orders(customerId, goodId, orderDate, numbersOfGoods) values (1, 2, '10-12-2020',2);                                                                       1);
insert into orders(customerId, goodId, orderDate, numbersOfGoods) values (2, 3, '15-06-2020',2);
insert into orders(customerId, goodId, orderDate, numbersOfGoods) values (1, 3, '20-12-2020',1);
insert into orders(customerId, goodId, orderDate, numbersOfGoods) values (2, 1, '15-02-2020',1);
insert into orders(customerId, goodId, orderDate, numbersOfGoods) values (3, 4, '12-12-2020',3);
insert into orders(customerId, goodId, orderDate, numbersOfGoods) values (1, 2, '10-12-2020',2);                                                                       1);
insert into orders(customerId, goodId, orderDate, numbersOfGoods) values (3, 3, '15-06-2020',2);

insert into customer(first_name, last_name) values ('John', 'Snow');
insert into customer(first_name, last_name) values ('Stanis', 'Baratheon');
insert into customer(first_name, last_name) values ('Addart', 'Stark');

insert into good(description, price, count) values ('Блэндер', 2300, 467);
insert into good(description, price, count) values ('Микроволновая печь', 4500, 145);
insert into good(description, price, count) values ('Холодильник', 24000, 47);
insert into good(description, price, count) values ('Чайник', 1700, 375);


-- Получить название всех товаров дешевле 4000
select description from good where price < 4000;

-- Получить список покупателей и количество купленных ими товаров
select id, first_name, (select count(*) from orders where customerId = customer.id) as purchasedGoods from customer;

-- Получить имена покупателей, купивших товар с id = 3
select first_name from customer where id in (
    select customerId
    from orders
    where goodId = 3
);

-- Получить имена всех покупателей, купивших товар с названием 'Блендер'
select first_name from customer where id in (
    select customerId
    from orders
    where goodId in (
        select id
        from good
        where description = 'Блендер'
    )
);

-- Получить информацию по каждой покупке
select * from customer a full join orders o on a.id = o.customerId;



