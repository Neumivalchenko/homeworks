package ru.pcs.attestation.forms;

import lombok.Data;
import ru.pcs.attestation.models.Product;
import ru.pcs.attestation.models.User;

import java.time.LocalDateTime;

@Data
public class OrderForm {
    private LocalDateTime dt_created;
    private Product product;
    private User user;
}
