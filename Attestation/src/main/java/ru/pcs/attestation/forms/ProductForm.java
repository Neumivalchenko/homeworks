package ru.pcs.attestation.forms;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductForm {
    private String name;
    private BigDecimal price;
    private String description;
}
