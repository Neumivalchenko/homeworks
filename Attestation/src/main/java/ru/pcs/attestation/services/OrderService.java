package ru.pcs.attestation.services;

public interface OrderService {
    void addOrder(Integer userId, Integer productId);

    void deleteById(Integer orderId);
}
