package ru.pcs.attestation.services;
import ru.pcs.attestation.forms.ProductForm;
import ru.pcs.attestation.models.Product;


import java.util.List;

public interface ProductsService {
    void addProduct(ProductForm form);
    List<Product> getAllProducts();

    void deleteProduct(Integer productId);

    Product getProduct(Integer productId);


    void update(Integer productId, ProductForm productForm);

}
