package ru.pcs.attestation.services;


import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.pcs.attestation.forms.UserForm;
import ru.pcs.attestation.models.Order;
import ru.pcs.attestation.models.User;
import ru.pcs.attestation.repositories.OrderRepository;
import ru.pcs.attestation.repositories.UsersRepository;

import java.util.List;

@RequiredArgsConstructor
@Component
public class UsersServiceImpl implements UsersService {

    private final OrderRepository orderRepository;
    public final PasswordEncoder passwordEncoder;
    private final UsersRepository usersRepository;

    @Override
    public void addUser(UserForm form) {
        User user = User.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .build();

        usersRepository.save(user);
    }

    @Override
    public List<User> getAllUsers() {
        return usersRepository.findAll();
    }

    @Override
    public void deleteUser(Integer userId, List<Order> orders) {
        for(Order order : orders){
            orderRepository.deleteById(order.getId());
        }
        usersRepository.deleteById(userId);
    }

    @Override
    public User getUser(Integer userId) {
        return usersRepository.getById(userId);
    }

    @Override
    public void update(Integer userId, UserForm userForm) {
        User user = usersRepository.getById(userId);
        user.setFirstName(userForm.getFirstName());
        user.setLastName(userForm.getLastName());
        user.setAddress(userForm.getAddress());
        user.setHashPassword(passwordEncoder.encode(userForm.getPassword()));

        usersRepository.save(user);
    }


}
