package ru.pcs.attestation.services;

import ru.pcs.attestation.forms.UserForm;
import ru.pcs.attestation.models.Order;
import ru.pcs.attestation.models.User;

import java.util.List;

public interface UsersService {
    void addUser(UserForm form);
    List<User> getAllUsers();

    void deleteUser(Integer userId, List<Order> orders);

    User getUser(Integer userId);


    void update(Integer userId, UserForm userForm);
    }
