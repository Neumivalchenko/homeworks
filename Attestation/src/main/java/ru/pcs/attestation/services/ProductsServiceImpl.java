package ru.pcs.attestation.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.pcs.attestation.forms.ProductForm;
import ru.pcs.attestation.models.Product;
import ru.pcs.attestation.repositories.ProductRepository;

import java.util.List;

@RequiredArgsConstructor
@Component
public class ProductsServiceImpl implements ProductsService {

    private final ProductRepository productRepository;

    @Override
    public void addProduct(ProductForm form) {
        Product product = Product.builder()
                .name(form.getName())
                .price(form.getPrice())
                .Description(form.getDescription())
                .build();
        productRepository.save(product);
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public void deleteProduct(Integer productId) {
        productRepository.deleteById(productId);
    }

    @Override
    public Product getProduct(Integer productId) {
        return productRepository.getById(productId);
    }

    @Override
    public void update(Integer productId, ProductForm productForm) {
        Product product = productRepository.getById(productId);
        product.setPrice(productForm.getPrice());
        product.setName(productForm.getName());
        product.setDescription(productForm.getDescription());
        productRepository.save(product);
    }


}
