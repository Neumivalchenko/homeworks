package ru.pcs.attestation.services;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.pcs.attestation.models.Order;


import ru.pcs.attestation.repositories.OrderRepository;

import java.time.LocalDateTime;

@RequiredArgsConstructor
@Component
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final UsersService usersService;
    private final ProductsService productsService;


    @Override
    public void addOrder(Integer userId, Integer productId) {
        Order order = Order.builder()
                .dt_created(LocalDateTime.now())
                .user(usersService.getUser(userId))
                .product(productsService.getProduct(productId))
                .build();

        orderRepository.save(order);
    }

    @Override
    public void deleteById(Integer orderId) {
        orderRepository.deleteById(orderId);
    }
}
