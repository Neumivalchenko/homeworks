package ru.pcs.attestation.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.attestation.models.Order;
import ru.pcs.attestation.models.Product;
import ru.pcs.attestation.models.User;

import java.util.List;


public interface OrderRepository extends JpaRepository<Order, Integer> {
    List<Order> findOrdersByUser(User user);
}
