package ru.pcs.attestation.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.pcs.attestation.forms.UserForm;
import ru.pcs.attestation.models.Order;
import ru.pcs.attestation.models.User;
import ru.pcs.attestation.repositories.OrderRepository;
import ru.pcs.attestation.services.UsersService;

import java.util.List;

@RequiredArgsConstructor
@Controller
public class UsersController {

    private final OrderRepository orderRepository;
    private final UsersService usersService;


    @GetMapping("/users")
    public String getUsersPage(Model model) {
        List<User> users = usersService.getAllUsers();
        model.addAttribute("users", users);
        return "users";
    }

    @GetMapping("/users/{user-id}")
    public String getUserPage(Model model, @PathVariable("user-id") Integer userId) {
        User user = usersService.getUser(userId);
        model.addAttribute("user", user);
        List<Order> orders = orderRepository.findOrdersByUser(user);
        model.addAttribute("orders", orders);
        return "user";
    }
    @PostMapping("/users")
    public String addUser(UserForm form, BindingResult result, RedirectAttributes forRedirectModel) {
        if (result.hasErrors()) {
            forRedirectModel.addFlashAttribute("errors", "Есть ошибки на форме!");
            return "redirect:/users";
        }
        usersService.addUser(form);
        return "redirect:/users";
    }

    @PostMapping("/user/{user-id}/delete")
    public String deleteUser(@PathVariable("user-id") Integer userId) {
        List<Order> orders = orderRepository.findOrdersByUser(usersService.getUser(userId));
        usersService.deleteUser(userId, orders);
        return "redirect:/users";
    }

    @PostMapping("/profile/{user-id}/update")
    public String update(Model model,@PathVariable("user-id") Integer userId, UserForm userForm) {
        usersService.update(userId, userForm);
        User user = usersService.getUser(userId);
        model.addAttribute("user", user);
        List<Order> orders = orderRepository.findOrdersByUser(user);
        model.addAttribute("orders", orders);
        return "redirect:/profile/{user-id}";
    }

    @GetMapping("/profile/{user-id}")
    public String profile(Model model, @PathVariable("user-id") Integer userId){
        User user = usersService.getUser(userId);
        model.addAttribute("user", user);
        List<Order> orders = orderRepository.findOrdersByUser(user);
        model.addAttribute("orders", orders);
        return "profile";
    }

}
