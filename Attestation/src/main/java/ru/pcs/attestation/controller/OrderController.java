package ru.pcs.attestation.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.pcs.attestation.models.Order;
import ru.pcs.attestation.models.User;
import ru.pcs.attestation.repositories.OrderRepository;
import ru.pcs.attestation.services.OrderService;
import ru.pcs.attestation.services.UsersService;

import java.util.List;


@RequiredArgsConstructor
@Controller
public class OrderController {

    private final OrderRepository orderRepository;
    private final UsersService usersService;
    private final OrderService orderService;


    @PostMapping("/products/{product-id}_{user-id}/order")
    public String addOrder(@PathVariable("product-id") Integer productId,
                           @PathVariable("user-id") Integer userId){
        orderService.addOrder(userId, productId);
        return "redirect:/products";
    }

    @PostMapping("/profile/{order-id}/{user-id}/delete")
    public String deleteOrderFromProfile(Model model,
                           @PathVariable("order-id") Integer orderId,
                           @PathVariable("user-id") Integer userId){
        User user = usersService.getUser(userId);
        model.addAttribute("user", user);
        List<Order> orders = orderRepository.findOrdersByUser(user);
        model.addAttribute("orders", orders);
        orderService.deleteById(orderId);
        return "redirect:/profile/{user-id}";
    }

    @PostMapping("/order/{user-id}/{order-id}/delete")
    public String deleteOrder(Model model, @PathVariable("order-id") Integer orderId){
        List<User> users = usersService.getAllUsers();
        model.addAttribute("users", users);
        orderService.deleteById(orderId);
        return "users";
    }
}
