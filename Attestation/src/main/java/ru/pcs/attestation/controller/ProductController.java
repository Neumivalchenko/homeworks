package ru.pcs.attestation.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.pcs.attestation.forms.ProductForm;
import ru.pcs.attestation.models.Product;
import ru.pcs.attestation.models.User;
import ru.pcs.attestation.repositories.UsersRepository;
import ru.pcs.attestation.services.ProductsService;

import java.util.List;

@RequiredArgsConstructor
@Controller
public class ProductController {

    private final ProductsService productsService;
    private final UsersRepository usersRepository;

    @GetMapping("/products")
    public String getProductsPage(Model model, Authentication authentication){
        List<Product> products = productsService.getAllProducts();
        model.addAttribute("products", products);
        User user = usersRepository.findByEmail(authentication.getName()).orElseThrow(() -> new UsernameNotFoundException("User not found."));
        model.addAttribute("user", user);
        return "products";
    }


    @GetMapping("/productsEdit/{product-id}")
    public String edit(Model model, @PathVariable("product-id") Integer productId){
        Product product = productsService.getProduct(productId);
        model.addAttribute("product", product);
        return "edit";
    }

    @GetMapping("/productsEdit")
    public String productsEdit(Model model) {
        List<Product> products = productsService.getAllProducts();
        model.addAttribute("products", products);
        return "productsEdit";
    }

    @PostMapping("productsEdit/{product-id}/delete")
    public String deleteProduct(@PathVariable("product-id") Integer productId){
        productsService.deleteProduct(productId);
        return "redirect:/productsEdit";
    }

    @PostMapping("/productsEdit/add")
    public String addProduct(ProductForm form){
        productsService.addProduct(form);
        return "redirect:/productsEdit";
    }

    @PostMapping("/productsEdit/{product-id}/update")
    public String update(@PathVariable("product-id") Integer productId, ProductForm productForm){
        productsService.update(productId, productForm);
        return "redirect:/productsEdit";
    }
}
