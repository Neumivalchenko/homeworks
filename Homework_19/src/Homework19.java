import java.util.List;

public class Homework19 {
    public static void main(String[] args) {

        UsersRepositiry usersRepository = new UsersRepositoryFileImpl("users.txt");
        List<User> users = usersRepository.findByAge(22);
        List<User> workUsers = usersRepository.findByIsWorkerIsTrue();
        for(User user : users){
            System.out.println(user.getName() + ", возраст - " + user.getAge() + ", статус - " + workerStatus(user.isWorker()));
        }
        System.out.println();

        for(User user : workUsers){
            System.out.println(user.getName() + ", возраст - " + user.getAge() + ", статус - " + workerStatus(user.isWorker()));
        }
    }


    public static String workerStatus(boolean a){
        return a == true ? "Работает" : "Безработный";
    }

}
