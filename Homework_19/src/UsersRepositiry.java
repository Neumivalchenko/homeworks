import java.util.List;

public interface UsersRepositiry {
    List<User> findAll();
    void save(User user);
    List<User> findByAge(int age);
    List<User> findByIsWorkerIsTrue();
}
