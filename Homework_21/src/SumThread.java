import com.sun.tools.javac.Main;

public class SumThread extends Thread{
    int from;
    int to;
    int count = 0;

    @Override
    public void run() {
        for (int i = from; i < to; i++) {
            Homework.sums[count] += Homework.array[i];
        }
        count++;
    }


    public SumThread(int from, int to) {
        this.from = from;
        this.to = to;
    }
}
