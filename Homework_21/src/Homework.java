import java.util.Random;
import java.util.Scanner;


public class Homework {
    public static int[] sums;
    public static int sum;
    public static int[] array;


    public static void main(String[] args) throws InterruptedException {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество случайных чисел для суммирования (например: 2000000): ");
        int numbersCount = scanner.nextInt();
        System.out.println("Введите количество потоков для обработки (например: 5): ");
        int threadCount = scanner.nextInt();


        array = new int[numbersCount];
        sums = new int[threadCount];
// Вычисление с помощбю цикла в потоке main
        for (int i = 0; i < numbersCount; i++) {
            array[i] = random.nextInt(100);
        }

        int realSum = 0;

        for (int i = 0; i < numbersCount; i++) {
            realSum += array[i];
        }

        System.out.println("Действительная сумма: " + realSum);
// Многопоточное вычисление

// Создание заданного числа потоков без последнего
        for (int i = 0; i < threadCount; i++) {
            SumThread thread = new SumThread(numbersCount / threadCount * i, numbersCount / threadCount * (i + 1));
            thread.start();
            thread.join();
        }
// Создание последнего потока
        SumThread finalThread = new SumThread((numbersCount - (numbersCount % threadCount)), numbersCount);
        finalThread.start();
        finalThread.join();

        for (int i = 0; i < threadCount; i++) {
            sum += sums[i];
        }
        
        System.out.println("Сумма в многопоточности: " + sum);
    }
}
